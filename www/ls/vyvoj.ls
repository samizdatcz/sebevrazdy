container = ig.containers.vyvoj
return unless container
container = d3.select container
scaleX = d3.scale.linear!
  ..domain [54 0]
  # ..range [0 1]
scaleY = d3.scale.linear!
  ..domain [40 0]
data = d3.tsv.parse ig.data.vyvoj, (row) ->
  row.points = [1, 2].map ->
    year = parseInt row["year#it"], 10
    value = parseFloat row["value#it"]
    x = scaleX year
    labelX = year
    y = scaleY value
    labelY = ig.utils.formatNumber value, 1
    {x, y, labelX, labelY, year, value}
  row.points.0.x = scaleX row.points.1.year - row.points.0.year
  row.points.1.x = scaleX 0
  row.points.1.labelY += " – #{row.country}"
  row

config = {}
  ..padding = {top: 20, right: 305, bottom: 30, left: 30}
  ..width = 610
  ..height = 600
  ..data = data
highlightCountries = <[ Korea Litva Estonsko Lotyšsko ]>
lineChart = new ig.LineChart container, config
  ..lines.pointG.append \text
    ..text -> it.labelY
    ..attr \x (d, i) -> if i then 10 else -10
    ..attr \y 4
    ..attr \text-anchor (d, i) -> if i then "start" else "end"
  ..lines.lineG
    ..classed \highlight -> it.country in highlightCountries
    ..classed \increasing -> it.points.1.y > it.points.0.y
class Group
  (@from, @to, @text) ->
    @top = lineChart.scaleY scaleY @from
    @bottom = lineChart.scaleY scaleY @to
    @top -= 5
    @bottom += 5
    @height = @bottom - @top


groups =
  new Group 29.5 29.1 "Litva a Korea"
  new Group 21 20.4 "Rusko a Lotyšsko"
  new Group 19.4 18.7 "Maďarsko a Japonsko"
  new Group 17.4 16.6 "Belgie a Estonsko"
  new Group 15.8 15.3 "Finsko, Francie a Polsko"
  new Group 14.2 13.6 "Česko a Rakousko"
  new Group 12.5 10.1 "Spojené státy, Švédsko, Švýcarsko, Island, Dánsko, Nový Zéland, Irsko, Chile, Německo, Norsko, Nizozemsko, Kanada a Austrálie"
  new Group 8.7 8.7 "Portugalsko"
  new Group 7.6 7.3 "Anglie, Španělsko, Lucembursko"
  new Group 6.4 5.8 "Izrael, Itálie, Brazílie, "
  new Group 5.0 4.2 "Mexiko, Kolumbie, Řecko"
  new Group 1.3 1.3 "Jižní Afrika"

container.append \h2
  ..html "Sebevražednost v Pobaltí klesla, v Koreji naopak stoupá"
container.append \div
  ..attr \class \groups
  ..selectAll \div .data groups .enter!append \div
    ..attr \class \group
    ..style \top -> "#{it.top}px"
    ..style \height -> "#{it.height}px"
    ..append \span
      ..html (.text)
